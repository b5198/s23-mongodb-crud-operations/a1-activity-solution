db.users.insertMany([
		{
			"firstName": "Yeji",
			"lastName": "Hwang",
			"email": "yeji@itzy.com",
			"password": "dalla",
			"isAdmin": false
		},
		{
			"firstName": "Choi",
			"lastName": "Jisu",
			"email": "lia@itzy.com",
			"password": "icy",
			"isAdmin": false
		},
		{
			"firstName": "Ryujin",
			"lastName": "Shin",
			"email": "ryujin@itzy.com",
			"password": "wannabe",
			"isAdmin": false
		},
		{
			"firstName": "Yuna",
			"lastName": "Shin",
			"email": "yuna@itzy.com",
			"password": "loco",
			"isAdmin": false
		},
		{
			"firstName": "Chae Ryeong",
			"lastName": "Lee",
			"email": "ryeongchae@itzy.com",
			"password": "voltage",
			"isAdmin": false
		},
	])

db.courses.insertMany([
		{
			"name": "JavaScript 101",
			"price": 5000,
			"isActive": false
		},
		{
			"name": "HTML 101",
			"price": 2000,
			"isActive": false
		},
		{
			"name": "CSS 101",
			"price": 2500,
			"isActive": false
		}
	])

db.users.find({isAdmin: false})

db.users.updateOne({},{$set:{isAdmin:true}})
db.courses.updateOne({"name": "JavaScript 101"},{$set:{isActive:true}})

db.courses.deleteMany({isActive:false})